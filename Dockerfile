FROM ubuntu
# see https://stackoverflow.com/questions/44331836/apt-get-install-tzdata-noninteractive
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install --yes texlive-xetex texlive-latex-extra python3-pygments